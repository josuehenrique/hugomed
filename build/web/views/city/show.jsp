<%-- 
    Document   : novo
    Created on : 23/11/2013, 14:16:39
    Author     : josue
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>HugoMed - Nova Cidade</title>

        <meta name="description" content="Common form elements and layouts" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!--basic styles-->

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

        <link rel="stylesheet" href="assets/css/jquery-ui-1.10.3.custom.min.css" />
        <link rel="stylesheet" href="assets/css/chosen.css" />
        <link rel="stylesheet" href="assets/css/colorpicker.css" />

        <link rel="stylesheet" href="assets/css/ace-fonts.css" />

        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-responsive.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />

        <script src="assets/js/ace-extra.min.js"></script>
    </head>

    <body>
        <div class="navbar" id="navbar">
            <script type="text/javascript">
                try {
                    ace.settings.check('navbar', 'fixed')
                } catch (e) {
                }
            </script>
            <%@include file="../../views/layout/title.jsp" %>
        </div>

        <div class="main-container container-fluid">

            <%@include file="../../views/layout/menu_left.jsp" %>

            <div class="main-content">
                <div class="breadcrumbs" id="breadcrumbs">
                    <script type="text/javascript">
                        try {
                            ace.settings.check('breadcrumbs', 'fixed')
                        } catch (e) {
                        }
                    </script>

                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home home-icon"></i>
                            <a href="servletmain?business=UserAction&action=index">Home</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                            </span>
                        </li>

                        <li>
                            <a href="servletmain?business=StateAction&action=list">Estados</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                            </span>
                        </li>
                        
                        <li>
                            <a href="servletmain?business=CityAction&action=list">Cidades</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                            </span>
                        </li>
                        <li class="active">Cidade: ${objCity.name} </li>
                    </ul><!--.breadcrumb-->
                </div>
               
                <div class="page-content">
                    <div class="page-header position-relative">
                        <h1>
                            Cidade: ${objCity.name} 
                        </h1>
                    </div><!--/.page-header-->


                    <div class="row-fluid">
                        <div class="span12">
                            <!--PAGE CONTENT BEGINS-->
                               <div class="control-group">
                                    <b style="width:20px"> Nome: </b> ${objCity.name}
                                </div>

                                <div class="form-actions">
                                    <a href="servletmain?business=CityAction&action=edit&state_id=<%= request.getParameter("state_id") %>&id=${objCity.id}" 
                                       class="btn btn-warning ">
                                        <i class="icon-edit bigger-110"></i>
                                        Editar
                                    </a>
                                    
                                    &nbsp; &nbsp; &nbsp;
                                    
                                    <a href="servletmain?business=CityAction&action=list&state_id=<%= request.getParameter("state_id") %>" 
                                       class="btn ">
                                        <i class="icon-undo bigger-110"></i>
                                        Voltar
                                    </a>
                                </div>
                                <div class="hr"></div>
                                <div class="space-24"></div>
                                <hr />
                        </div><!--/.span-->
                    </div><!--/.row-fluid-->
                </div><!--/.page-content-->
            </div><!--/.main-content-->
        </div><!--/.main-container-->

        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
            <i class="icon-double-angle-up icon-only bigger-110"></i>
        </a>

        <script type="text/javascript">
            window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
        </script>

        <script type="text/javascript">
            if ("ontouchend" in document)
                document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>
