<%-- 
    Document   : novo
    Created on : 23/11/2013, 14:16:39
    Author     : josue
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>HugoMed -  
            <c:if test="${objCity.id == 0}"> Adicionar </c:if>
            <c:if test="${objCity.id != 0}"> Editar </c:if> 
            Estado</title>

        <meta name="description" content="Common form elements and layouts" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!--basic styles-->

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
        
        <link rel="stylesheet" href="assets/css/jquery-ui-1.10.3.custom.min.css" />
        <link rel="stylesheet" href="assets/css/chosen.css" />

        <link rel="stylesheet" href="assets/css/ace-fonts.css" />

        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-responsive.min.css" />

        <script src="assets/js/ace-extra.min.js"></script>
    </head>

    <body>
        <div class="navbar" id="navbar">
            <script type="text/javascript">
                try {
                    ace.settings.check('navbar', 'fixed')
                } catch (e) {
                }
            </script>
            <%@include file="../../views/layout/title.jsp" %>
        </div>

        <div class="main-container container-fluid">

            <%@include file="../../views/layout/menu_left.jsp" %>

            <div class="main-content">
                <div class="breadcrumbs" id="breadcrumbs">
                    <script type="text/javascript">
                        try {
                            ace.settings.check('breadcrumbs', 'fixed')
                        } catch (e) {
                        }
                    </script>

                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home home-icon"></i>
                            <a href="servletmain?business=UserAction&action=index">Home</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                            </span>
                        </li>

                        <li>
                            <a href="servletmain?business=StateAction&action=list">Estados</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                            </span>
                        </li>
                        
                        <li>
                            <a href="servletmain?business=CityAction&action=list&state_id=<%= request.getParameter("state_id") %>">
                                ${stateName} 
                                <label class="icon-double-angle-right "></label>
                                Cidades</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                            </span>
                        </li>
                        <li class="active">
                            <c:if test="${objCity.id == 0}"> Adicionar </c:if>
                            <c:if test="${objCity.id != 0}"> Editar </c:if>
                         Cidade</li>
                    </ul><!--.breadcrumb-->
                </div>
                 
                ${flash}
                <c:remove var="flash" scope="session" />
                
                <div class="page-content">
                    <div class="page-header position-relative">
                        <h1>
                            <c:if test="${objCity.id == 0}"> Adicionar </c:if>
                            <c:if test="${objCity.id != 0}"> Editar </c:if>
                         Cidade
                        </h1>
                    </div><!--/.page-header-->

                    <div class="row-fluid">
                        <div class="span12">
                            <!--PAGE CONTENT BEGINS-->
                            <form class="form-horizontal" action="servletmain" method="post">
                                <input type="hidden" name="business" value="CityAction" />
                                <input type="hidden" name="action" value="save" />
                                <input type="hidden" name="id" value="${objCity.id}" />
                                <input type="hidden" name="state_id" value="<%= request.getParameter("state_id") %>" />
                                <% session.setAttribute("submitIsOk","true"); %>

                                <div class="control-group">
                                    <label class="control-label" for="form-field-1">Nome </label>

                                    <div class="controls">
                                        <input type="text" value="${objCity.name}" name='name' id="name"  />
                                    </div>
                                </div>

                                <div class="form-actions">

                                    <button onclick="return true;" class="btn btn-info" type='submit'>
                                        <i class="icon-ok bigger-110"></i>
                                        Salvar
                                    </button>

                                    &nbsp; &nbsp; &nbsp;
                                    <a href="servletmain?business=CityAction&action=list&state_id=<%= request.getParameter("state_id") %>" class="btn ">
                                        <i class="icon-undo bigger-110"></i>
                                        Voltar
                                    </a>
                                </div>

                                <div class="hr"></div>

                                <div class="space-24"></div>
                                <hr />
                            </form>
                        </div><!--/.span-->
                    </div><!--/.row-fluid-->
                </div><!--/.page-content-->
            </div><!--/.main-content-->
        </div><!--/.main-container-->

        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
            <i class="icon-double-angle-up icon-only bigger-110"></i>
        </a>

        <script type="text/javascript">
            window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
        </script>

        <script type="text/javascript">
            if ("ontouchend" in document)
                document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="assets/js/bootstrap.min.js"></script>       
        
        <script src="assets/js/chosen.jquery.min.js"></script>
        <script src="assets/js/bootstrap-colorpicker.min.js"></script>
        <script src="assets/js/jquery.autosize-min.js"></script>
        <script src="assets/js/bootstrap-tag.min.js"></script>

        <script src="assets/js/ace-elements.min.js"></script>
        <script src="assets/js/ace.min.js"></script>
        
    </body>
</html>
