<%-- 
    Document   : novo
    Created on : 23/11/2013, 14:16:39
    Author     : josue
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <script src="assets/js/ajax.js"></script>
    <script>
      function getCities() {
        var state = document.getElementById("select_states").value;
        ajax('servletmain?business=CityAction&action=getCities&state=' + state, "select_cities");
      }
    </script>
    <meta charset="utf-8" />
    <title>HugoMed -     
      <c:if test="${objEmployee.id == 0}"> Adicionar </c:if>
      <c:if test="${objEmployee.id != 0}"> Editar </c:if>
        Funcionário
      </title>

      <meta name="description" content="Common form elements and layouts" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />

      <!--basic styles-->

      <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
      <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
      <link rel="stylesheet" href="assets/css/jquery-ui-1.10.3.custom.min.css" />
      <link rel="stylesheet" href="assets/css/chosen.css" />
      <link rel="stylesheet" href="assets/css/ace-fonts.css" />
      <link rel="stylesheet" href="assets/css/ace.min.css" />
      <link rel="stylesheet" href="assets/css/ace-responsive.min.css" />
      <script src="assets/js/ace-extra.min.js"></script>
      <script type="text/javascript" src="assets/js/jquery-2.0.3.min.js"></script>
      <script type="text/javascript" src="assets/js/jquery.maskedinput.min.js"/></script>
    <script type="text/javascript">
     $(document).ready(function () {
       $("#cpf").mask("999 . 999 . 999 - 99");
       $("#cep").mask("99 . 999 - 999");
       $("#phone").mask("( 99 ) 9999 - 9999");
       $("#dt").mask("99 / 99 / 9999");
     });
    </script>

    <script type="text/javascript">
      function RemoveMasksAndSubmit() {
        $('#cep').mask("99999999");
        $('#phone').mask("9999999999");
        $('#cpf').mask("99999999999");
        $('#dt').mask("99/99/9999");

        $('#form').submit();
      }
    </script>
  </head>

  <body>
    <div class="navbar" id="navbar">
      <script type="text/javascript">
        try {
          ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
      </script>
    <%@include file="../../views/layout/title.jsp" %>
  </div>

  <div class="main-container container-fluid">

    <%@include file="../../views/layout/menu_left.jsp" %>

    <div class="main-content">
      <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
          try {
            ace.settings.check('breadcrumbs', 'fixed')
          } catch (e) {
          }
        </script>

        <ul class="breadcrumb">
          <li>
            <i class="icon-home home-icon"></i>
            <a href="servletmain?business=EmployeeAction&action=index">Home</a>

            <span class="divider">
              <i class="icon-angle-right arrow-icon"></i>
            </span>
          </li>

          <li>
            <a href="servletmain?business=EmployeeAction&action=list">Funcionários</a>

            <span class="divider">
              <i class="icon-angle-right arrow-icon"></i>
            </span>
          </li>
          <li class="active">
            <c:if test="${objEmployee.id == 0}"> Adicionar </c:if>
            <c:if test="${objEmployee.id != 0}"> Editar </c:if>
              Funcionário</li>
          </ul><!--.breadcrumb-->
        </div>

      ${flash}      
      <c:remove var="flash" scope="session" />

      <div class="page-content">
        <div class="page-header position-relative">
          <h1>
            <c:if test="${objEmployee.id == 0}"> Adicionar </c:if>
            <c:if test="${objEmployee.id != 0}"> Editar </c:if>
              Funcionário
            </h1>
          </div><!--/.page-header-->

          <div class="row-fluid">
            <div class="span12">
              <!--PAGE CONTENT BEGINS-->
              <form id="form" class="form-horizontal" action="servletmain" method="POST">
                <input type="hidden" name="business" value="EmployeeAction" />
                <input type="hidden" name="action" value="save" />
                <input type="hidden" name="id" value="${objEmployee.id}" />
              <% session.setAttribute("submitIsOk", "true");%>


              <!--DADOS GERAIS -->
              <fieldset>
                <legend>Dados Gerais</legend>
                <div class="control-group">
                  <label class="control-label" for="form-field-1">Nome </label>

                  <div class="controls">
                    <input type="text" value="${objEmployee.name}" name="name" id="name"  />
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for="form-field-1">Genero</label>

                  <div class="controls">
                    <select name="gender">
                      <option value="0">Selecione um genero</option>
                      <option value="M" <c:if test="${objEmployee.gender == 'M'}"> selected="selected"</c:if> >Masculino</option>
                      <option value="F" <c:if test="${objEmployee.gender == 'F'}"> selected="selected"</c:if> >Feminino</option>
                      </select>
                    </div>
                  </div>

                  <div class="control-group">
                    <label class="control-label" for="form-field-1">RG </label>

                    <div class="controls">
                      <input type="text" value="${objEmployee.rg}" name='rg' id="rg"  />
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for="form-field-1">CPF </label>

                  <div class="controls">
                    <input type="text" value="${objEmployee.cpf}" name='cpf' id="cpf"/>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label " for="form-field-1" style="padding-right: 20px;">
                    Telefone
                  </label>

                  <div class="input-prepend">
                    <span class="add-on">
                      <i class="icon-phone"></i>
                    </span>

                    <input class="input-medium " type="text" value="${objEmployee.phone}"  name='phone' id="phone">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label " for="form-field-1" style="padding-right: 20px;">
                    Data de Nascimento
                  </label>

                  <div class="input-prepend">
                    <span class="add-on">
                      <i class="icon-calendar"></i>
                    </span>

                    <input class="input-medium " type="text" value="${objEmployee.dt_birth}"  name='dt_birth' id="dt">
                  </div>
                </div>

              </fieldset>
              <!--ENDEREÇO -->
              <fieldset>
                <legend>Endereço</legend>
                <div class="control-group">
                  <label class="control-label" for="form-field-1">CEP </label>

                  <div class="controls">
                    <input type="text" value="${objEmployee.cep}" name='cep' id="cep"  />
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for="form-field-1">Logradouro </label>

                  <div class="controls">
                    <input type="text" value="${objEmployee.street}" name='street' id="street"  />
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for="form-field-1">Bairro</label>

                  <div class="controls">
                    <input type="text" value="${objEmployee.neighborhood}" name='neighborhood' id="neighborhood"  />
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label " for="form-field-1">Numero</label>

                  <div class="controls">
                    <input class="span1" type="text" value="${objEmployee.number}"  name='number' id="number"  />
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for="form-field-1">Estado</label>

                  <div class="controls">
                    <select name="state_id" id="select_states" onchange="getCities();">
                      <option value="0">Selecione um estado</option>
                      <c:forEach var="item" items="${lstStates}" >
                        <option value="${item.id}" 
                                <c:if test="${item.id == state_id}"> selected="selected"</c:if>>
                          ${item.acronym} - ${item.name}
                        </option>
                      </c:forEach>
                    </select>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for="form-field-1">Cidade</label>

                  <div class="controls">
                    <select name="city_id" id="select_cities">
                      <option value="0">Selecione uma cidade</option>
                      <c:forEach items="${lstCities}" var="obj" >
                        <option value='${obj.id}'
                                <c:if test="${obj.id == city_id}"> selected="selected"</c:if>>
                          ${obj.name}
                        </option>
                      </c:forEach>
                    </select>
                  </div>
                </div>

              </fieldset>
              <!-- DADOS DO USUÁRIO-->           
              <fieldset> 
                <legend>Dados do Usuário</legend> 
                <div class="control-group">
                  <label class="control-label" for="form-field-1">Login</label>

                  <div class="controls">
                    <input type="text" value="${objUser.login}" name='user_login' maxlength="15" id="login"  />
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for="form-field-2">Senha</label>

                  <div class="controls">
                    <input type="password" value="${objUser.password}" name='user_password' id="password"  />
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for="form-field-2">Confirmação da Senha</label>

                  <div class="controls">
                    <input type="password" value="${objUser.password}" name='user_password_confirmation' id="password_confirmation"  />
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label"> Status: </label>
                  <div class="controls"> 
                    <label>
                      <input name="user_status" value="true" <c:if test="${objUser.status == true}"> checked="checked"</c:if> type="radio" class="ace">
                        <span class="lbl"> Ativo</span>
                      </label>

                      <label> 
                        <input name="user_status" value="false" <c:if test="${objUser.status == false}"> checked="checked"</c:if> type="radio" class="ace">
                        <span class="lbl"> Inativo</span>
                      </label>
                    </div>
                  </div>

                  <div class="control-group">
                    <label class="control-label"> Nível do Usuário: </label>
                    <div class="controls"> 
                      <label>
                        <input name="user_admin" value="true" <c:if test="${objUser.admin == true}"> checked="checked"</c:if> type="radio" class="ace">
                        <span class="lbl"> Administrador</span>
                      </label>

                      <label> 
                        <input name="user_admin" value="false" <c:if test="${objUser.admin == false}"> checked="checked"</c:if> type="radio" class="ace">
                      <span class="lbl"> Usuário</span>
                    </label>
                  </div>
                </div>
              </fieldset>

              <div class="form-actions">

                <button onclick="RemoveMasksAndSubmit();" class="btn btn-info" type='submit'>
                  <i class="icon-ok bigger-110"></i>
                  Salvar
                </button>

                &nbsp; &nbsp; &nbsp;
                <a href="servletmain?business=EmployeeAction&action=list" class="btn ">
                  <i class="icon-undo bigger-110"></i>
                  Voltar
                </a>
              </div>

              <div class="hr"></div>

              <div class="space-24"></div>
              <hr />
            </form>
          </div><!--/.span-->
        </div><!--/.row-fluid-->
      </div><!--/.page-content-->
    </div><!--/.main-content-->
  </div><!--/.main-container-->

  <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
    <i class="icon-double-angle-up icon-only bigger-110"></i>
  </a>

  <script type="text/javascript">
    window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
  </script>

  <script type="text/javascript">
    if ("ontouchend" in document)
      document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
  </script>
  <script src="assets/js/bootstrap.min.js"></script>

  <script src="assets/js/chosen.jquery.min.js"></script>
  <script src="assets/js/fuelux/fuelux.spinner.min.js"></script>
  <script src="assets/js/date-time/moment.min.js"></script>
  <script src="assets/js/bootstrap-colorpicker.min.js"></script>
  <script src="assets/js/jquery.autosize-min.js"></script>
  <script src="assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
  <script src="assets/js/bootstrap-tag.min.js"></script>
  <script src="assets/js/jquery-ui-1.10.3.full.min.js"></script>
  <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
  <script src="assets/js/ace-elements.min.js"></script>
  <script src="assets/js/ace.min.js"></script>
</body>
</html>
