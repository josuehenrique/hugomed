<%@page import="br.com.hugoMed.bean.User"%>

<ul class="nav ace-nav pull-right">
  
    <li class="light-blue">
        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
            <span class="user-info">
                <small>Bem Vindo(a),</small>
                <% User user = (User)session.getAttribute("objUserSS");   %>
                <%= user.getName() %>
            </span>

            <i class="icon-caret-down"></i>
        </a>

        <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
            <li>
                <a href="servletmain?business=UserAction&action=logout">
                    <i class="icon-off"></i>
                    Sair
                </a>
            </li>
        </ul>
    </li>   
</ul>