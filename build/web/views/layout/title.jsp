<%-- 
    Document   : title
    Created on : 22/11/2013, 00:51:07
    Author     : josue
--%>

<div class="navbar-inner">
    <div class="container-fluid">
        <a href="servletmain?business=UserAction&action=index" class="brand">
            <small>
                <i class="icon-leaf"></i>
                <b>HugoMed</b>
            </small>
        </a><!--/.brand-->
        <%@include file="welcome_user.jsp" %>
    </div><!--/.container-fluid-->
</div><!--/.navbar-inner-->
