

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<select name="city_id" id="select_cities">
    <option value="0">Selecione uma cidade</option>
    <c:forEach items="${listCities}" var="obj" >
        <option value='${obj.id}'>${obj.name}</option>
    </c:forEach>
</select>