
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Usuários</title>

        <meta name="description" content="Static &amp; Dynamic Tables" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

        <link rel="stylesheet" href="assets/css/ace-fonts.css" />

        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-responsive.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />

        <script src="assets/js/ace-extra.min.js"></script>
        <script> function confirmacao(id,name) { var resposta = confirm("Deseja remover o usuário: "+name+"?");        
                if (resposta == true) { window.location.href = "servletmain?business=UserAction&action=delete&id="+id;      } }   
         </script>
    </head>

    <body>
        <div class="navbar" id="navbar">
            <script type="text/javascript">
                try {
                    ace.settings.check('navbar', 'fixed')
                } catch (e) {
                }
            </script>
            <%@include file="../../views/layout/title.jsp" %>
        </div>

        <div class="main-container container-fluid">
            <a class="menu-toggler" id="menu-toggler" href="#">
                <span class="menu-text"></span>
            </a>
                <%@include file="../../views/layout/menu_left.jsp" %>
            <div class="main-content">
                <div class="breadcrumbs" id="breadcrumbs">
                    <script type="text/javascript">
                        try {
                            ace.settings.check('breadcrumbs', 'fixed')
                        } catch (e) {
                        }
                    </script>

                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home home-icon"></i>
                            <a href="servletmain?business=UserAction&action=index">Home</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                            </span>
                        </li>

                        <li> Usuários </li>
                    </ul><!--.breadcrumb-->
                </div>
                ${flash}
                <c:remove var="flash" scope="session" />
                <div class="page-content">
                    <div class="page-header position-relative">
                        <h1> Usuários </h1>
                    </div><!--/.page-header-->
                    <a href="servletmain?business=UserAction&action=new">
                    <button  class="btn btn-primary span2">
                        <i class="icon-plus bigger-125"></i>
                        Adicionar
                    </button>
                        </a>
                  <br />  
                  <br />  
                  <br />  
                    
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Login</th>
                                            <th width="40px">Ativo</th>
                                            <th width="40px">Admin</th>
                                            <th width="70px"></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    <c:forEach var="item" items="${lstUsers}" >
                                            <tr>
                                                <td>${item.name}</td>
                                                <td>${item.login}</td>
                                                <td style="text-align:center">   
                                                       <c:if test="${item.status == true}">
                                                          <i class="icon-ok-sign green"></i>
                                                       </c:if>
                                                    
                                                       <c:if test="${item.status == false}">
                                                          <i class="icon-remove-sign red"></i>
                                                       </c:if>
                                                </td>
                                                <td style="text-align:center">   
                                                       <c:if test="${item.admin== true}">
                                                          <i class="icon-ok-sign green "></i>
                                                       </c:if>
                                                    
                                                       <c:if test="${item.admin == false}">
                                                          <i class="icon-remove-sign red "></i>
                                                       </c:if>
                                                </td>
                                                <td>
                                                    <div class="hidden-phone visible-desktop action-buttons">
                                                        <a class="blue" href="servletmain?business=UserAction&action=show&id=${item.id}">
                                                            <i class="icon-eye-open bigger-130"></i>
                                                        </a>

                                                        <a class="green" href="servletmain?business=UserAction&action=edit&id=${item.id}">
                                                            <i class="icon-pencil bigger-130"></i>
                                                        </a>

                                                        <a class="red" href="javascript:func()" onclick="confirmacao(${item.id}, '${item.name}')">
                                                            <i class="icon-trash bigger-130"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div><!--PAGE CONTENT ENDS-->
                    </div><!--/.span-->
                </div><!--/.row-fluid-->
            </div><!--/.page-content-->
        </div><!--/.main-content-->
    </div><!--/.main-container-->

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
        <i class="icon-double-angle-up icon-only bigger-110"></i>
    </a>

    <script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
    </script>

    <script type="text/javascript">
        if ("ontouchend" in document)
            document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>
    <script src="assets/js/bootstrap.min.js"></script>

    <script src="assets/js/ace-elements.min.js"></script>
    <script src="assets/js/ace.min.js"></script>

</body>
</html>