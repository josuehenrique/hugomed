<%-- 
    Document   : menu_lateral
    Created on : 12/11/2015, 22:40:34
    Author     : josue
--%>
<a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-small btn-success">
                <i class="icon-signal"></i>
            </button>

            <button class="btn btn-small btn-info">
                <i class="icon-pencil"></i>
            </button>

            <button class="btn btn-small btn-warning">
                <i class="icon-group"></i>
            </button>

            <button class="btn btn-small btn-danger">
                <i class="icon-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!--#sidebar-shortcuts-->

    <ul class="nav nav-list">
        <li>
            <a href="#" class="dropdown-toggle">
                <i class="icon-cog"></i>
                <span class="menu-text"> Administra��o </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: ${open_administration}">
                <li class="${active_users}">
                    <a href="servletmain?business=UserAction&action=list">
                        <i class="icon-double-angle-right"></i>
                        Usu�rios
                    </a>
                </li>
                <li class="${active_employees}">
                    <a href="servletmain?business=EmployeeAction&action=list">
                        <i class="icon-double-angle-right"></i>
                        Funcion�rios
                    </a>
                </li>
                <li class="${active_states_and_cities}">
                    <a href="servletmain?business=StateAction&action=list">
                        <i class="icon-double-angle-right"></i>
                        Estados e Cidades
                    </a>
                </li>
            </ul>
        </li>

        <li class="${active_clients}">
            <a href="servletmain?business=ClientAction&action=list">
                <i class="icon-group"></i>
                <span class="menu-text"> Titulares </span>
            </a>
        </li>

        <li class="${active_health_plans}">
            <a href="servletmain?business=HealthPlanAction&action=list">
                <i class="icon- icon-credit-card"></i>
                <span class="menu-text"> Planos de Sa�de </span>
            </a>
        </li>

        <li>
            <a href="servletmain?business=ReportAction&action=list">
                <i class="icon-bar-chart"></i>
                <span class="menu-text"> Relat�rios </span>
            </a>
        </li>
    </ul><!--/.nav-list-->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>
</div>

<script type="text/javascript">
    try {
        ace.settings.check('sidebar', 'collapsed');
    } catch (e) {
    }
</script>




