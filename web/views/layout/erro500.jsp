<%
    if (request.getSession().getAttribute("objUsuarioSS") == null) {
        response.sendRedirect("");
    }
    else{
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>500 Error Page - HugoMed</title>

        <meta name="description" content="500 Error Page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!--basic styles-->

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

        <link rel="stylesheet" href="assets/css/ace-fonts.css" />

        <!--ace styles-->

        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-responsive.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />

        <script src="assets/js/ace-extra.min.js"></script>
    </head>

    <body>
        <div class="navbar" id="navbar">
            <script type="text/javascript">
                try {
                    ace.settings.check('navbar', 'fixed')
                } catch (e) {
                }
            </script>

            <%@include file="title.jsp" %>
        </div>
        <div class="main-container container-fluid">
            <a class="menu-toggler" id="menu-toggler" href="#">
                <span class="menu-text"></span>
            </a>
        <%@include file="menu_left.jsp" %>

            <div class="main-content">
                <div class="breadcrumbs" id="breadcrumbs">
                    <script type="text/javascript">
                        try {
                            ace.settings.check('breadcrumbs', 'fixed')
                        } catch (e) {
                        }
                    </script>
                  
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home home-icon"></i>
                            <a href="servletmain?business=UserAction&action=index">Home</a>

                            <span class="divider">
                                <i class="icon-angle-right arrow-icon"></i>
                            </span>
                        </li>

                        <li class="active">Erro 500</li>
                    </ul><!--.breadcrumb-->
                </div>

                <div class="page-content">
                    <div class="row-fluid">
                        <div class="span12">
                            <!--PAGE CONTENT BEGINS-->

                            <div class="error-container">
                                <div class="well">
                                    <h1 class="grey lighter smaller">
                                        <span class="blue bigger-125">
                                            <i class="icon-random"></i>
                                            500
                                        </span>
                                        Algo Deu Errado
                                    </h1>

                                    <hr />
                                    <h3 class="lighter smaller">
                                        Mas estamos trabalhando
                                        <i class="icon-wrench icon-animated-wrench bigger-125"></i>
                                        nisso!
                                    </h3>

                                    <div class="space"></div>

                                    <div>
                                        <h4 class="lighter smaller">Enquanto isso</h4>

                                        <ul class="unstyled spaced inline bigger-110">
                                            <li>
                                                <i class="icon-hand-right blue"></i>
                                                Leia o FAQ
                                            </li>

                                            <li>
                                                <i class="icon-hand-right blue"></i>
                                                Dê-nos mais informações sobre como ocorreu esse erro específico!
                                            </li>
                                        </ul>
                                    </div>

                                    <hr />
                                    <div class="space"></div>

                                    <div class="row-fluid">
                                        <div class="center">
                                            <a href="javascript:history.go(-1);" class="btn btn-grey">
                                                <i class="icon-arrow-left"></i>
                                                Voltar
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--PAGE CONTENT ENDS-->
                        </div><!--/.span-->
                    </div><!--/.row-fluid-->
                </div><!--/.page-content-->
            </div><!--/.main-content-->
        </div><!--/.main-container-->

        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
            <i class="icon-double-angle-up icon-only bigger-110"></i>
        </a>

        <script type="text/javascript">
            window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
        </script>

        <script type="text/javascript">
            if ("ontouchend" in document)
                document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="assets/js/bootstrap.min.js"></script>

        <script src="assets/js/ace-elements.min.js"></script>
        <script src="assets/js/ace.min.js"></script>
    </body>
</html>
<%}%>