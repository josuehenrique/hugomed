<%-- 
    Document   : application
    Created on : 21/11/2013, 22:23:18
    Author     : josue
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>HugoMed</title>

        <meta name="description" content="User login page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
        <link rel="stylesheet" href="assets/css/ace-fonts.css" />
        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-responsive.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />

        <script src="assets/js/ace-extra.min.js"></script>
    </head>

    <body class="login-layout">
        <div class="main-container container-fluid">
            <div class="main-content">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="login-container">
                            <div class="row-fluid">
                                <div class="center">
                                    <h1>
                                        <i class="icon-leaf green"></i>
                                        <span class="white"><b>HugoMed</b></span>
                                    </h1>
                                </div>
                            </div>

                            <div class="space-6"></div>

                            <div class="row-fluid">
                                <div class="position-relative">
                                    <div id="login-box" class="login-box visible widget-box no-border">
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <h4 class="header blue lighter bigger">
                                                    Por favor insira seus dados
                                                </h4>

                                                <div class="space-6"></div>

                                                <form action="servletmain" method="post">
                                                  <input type="hidden" name="business" value="UserAction" />
                                                    <input type="hidden" name="action" value="login" />
                                                    <fieldset>
                                                        <label>
                                                        <span class="block input-icon input-icon-right">
                                                                <input type="text" class="span12" name='login' placeholder="Usuário" />
                                                                <i class="icon-user"></i>
                                                            </span>
                                                        </label>

                                                        <label>
                                                            <span class="block input-icon input-icon-right">
                                                                <input type="password" class="span12" name='password' placeholder="Senha" />
                                                                <i class="icon-lock"></i>
                                                            </span>
                                                            <div style="color:red">${msg}</div>
                                                        </label>
                                                        
                                                        <div class="space"></div>

                                                        <div class="clearfix">
                                                            <button onclick="return true;" class="width-35 pull-right btn btn-small btn-primary" type='submit'>
                                                                <i class="icon-key"></i>
                                                                Autenticar
                                                            </button>
                                                        </div>

                                                        <div class="space-4"></div>
                                                    </fieldset>
                                                </form>
                                            </div><!--/widget-main-->
                                        </div><!--/widget-body-->
                                    </div><!--/login-box-->
                                </div><!--/position-relative-->
                            </div>
                        </div>
                    </div><!--/.span-->
                </div><!--/.row-fluid-->
            </div>
        </div><!--/.main-container-->
        
     <script type="text/javascript">
            window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
        </script>

        <script type="text/javascript">
            if ("ontouchend" in document)
                document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/ace-elements.min.js"></script>
        <script src="assets/js/ace.min.js"></script>
        </script>
    </body>
</html>