/*
## Copyright (C) 2008 Karlos Augusto <karlos@navesa.com.br>
## All rights reserved
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
## USA.
##
## Author(s): Karlos augusto <karlos@navesa.com.br>
*/


// Fun��o respons�vel de conectar a uma p�gina externa e retornar os resultados, no nosso caso a busca_item.php
function ajax(url,id){
    destino=id;
    req = null;
    // Procura por um objeto nativo (Mozilla/Safari)
    if (window.XMLHttpRequest){
    req = new XMLHttpRequest();
    req.onreadystatechange = processReqChange;
    req.open("GET",url,true);
    req.send(null);
    }
    // Procura por uma vers�o ActiveX (IE)
    else if (window.ActiveXObject){
    req = new ActiveXObject("Microsoft.XMLHTTP");
        if (req){
        req.onreadystatechange = processReqChange;
        req.open("GET",url,true);
        req.send();
        }
    }
}

function processReqChange(){
    // apenas quando o estado for "completado"
    if (req.readyState == 4){
    // apenas se o servidor retornar "OK"
        if (req.status ==200){
        // procura pela div id="pagina" e insere o conteudo
        // retornado nela, como texto HTML
        document.getElementById(destino).innerHTML = req.responseText;
        }
    }
}
