/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.hugoMed.action;

import br.com.hugoMed.bean.City;
import br.com.hugoMed.bean.Employee;
import br.com.hugoMed.bean.User;
import br.com.hugoMed.controller.BusinessLogic;
import br.com.hugoMed.persistence.CityDAO;
import br.com.hugoMed.persistence.EmployeeDAO;
import br.com.hugoMed.persistence.StateDAO;
import br.com.hugoMed.persistence.UserDAO;
import br.com.hugoMed.util.CriptografiaMD5;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author josue
 */
public class EmployeeAction implements BusinessLogic {
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");

        switch (action) {

            case "new": {
                aNew(request, response);
                break;
            }
            case "list": {
                aList(request, response);
                break;
            }
            case "save": {
                aSave(request, response);
                break;
            }
            case "show": {
                aShow(request, response);
                break;
            }
            case "edit": {
                aEdit(request, response);
                break;
            }
            case "delete": {
                aDelete(request, response);
                break;
            }
        }
    }  

    private boolean save(HttpServletRequest request) {
       
       Employee employee = new Employee();
       User user = new User();
       
       employee.setName(request.getParameter("name"));
       employee.setGender(request.getParameter("gender"));
       employee.setRg(request.getParameter("rg"));
       employee.setCpf(request.getParameter("cpf"));
       employee.setPhone(request.getParameter("phone"));
       DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            employee.setDt_birth(df.parse(request.getParameter("dt_birth")));
        } catch (ParseException ex) {
            Logger.getLogger(EmployeeAction.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       employee.setCep(request.getParameter("cep"));
       employee.setNeighborhood(request.getParameter("neighborhood"));
       employee.setStreet(request.getParameter("street"));
       employee.setNumber(request.getParameter("number"));
       employee.setCity_id(Integer.parseInt(request.getParameter("city_id")));
       
       user.setName(request.getParameter("name"));
       user.setLogin(request.getParameter("user_login"));
       user.setPassword(CriptografiaMD5.encrypt(request.getParameter("user_password")));
       user.setStatus(Boolean.parseBoolean(request.getParameter("user_status")));
       user.setAdmin(Boolean.parseBoolean(request.getParameter("user_admin")));
       
         System.out.println("Encontrado o funcionario de id"+String.valueOf(employee.getId())); 
         if (employee.getId() == 0) {
             if (new UserDAO().incluir(user)){ System.out.println("Incluiu o Usuário "+user.getName());}
             int user_id = new UserDAO().getUserId(user.getName(),user.getLogin());
             System.out.println("Encontrado o usuario de id"+String.valueOf(user_id)); 
             employee.setUser_id(user_id);
             if (new EmployeeDAO().create(employee)){ System.out.println("Incluiu o Funcionario "+employee.getName());}
        } else {
             new UserDAO().alterar(user);
             new EmployeeDAO().update(employee);
        }
       
     return true;
    }
 
    private String flashError(String msg) {
        String mensagem = "<div class='alert alert-block alert-error'>"
                + "<button type = 'button' class='close' data-dismiss='alert'>"
                + "<i class='icon-remove'></i></button>"
                + "<p><strong><i class='icon-remove'></i> ERRO!!!  </strong>"
                + msg
                + "</p></div>";
        return mensagem;
    }

    private String flashSucess(String msg) {
        String mensagem = "<div class='alert alert-block alert-success'>"
                + "<button type = 'button' class='close' data-dismiss='alert'>"
                + "<i class='icon-remove'></i></button>"
                + "<p><strong><i class='icon-ok'></i> Sucesso!!!  </strong>"
                + msg
                + "</p></div>";
        return mensagem;
    }

    private void aEdit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        Employee employee = new EmployeeDAO().getEmployee(id);
        City city = new CityDAO().getCity(employee.getCity_id());
        
        request.setAttribute("objEmployee", employee);
        request.setAttribute("objUser",new UserDAO().getUser(employee.getUser_id()));
        request.setAttribute("lstStates", new StateDAO().getStates());
        request.setAttribute("lstCities", new CityDAO().getCities(city.getState_id()));
        request.setAttribute("state_id", new StateDAO().getState(city.getState_id()).getId());
        request.setAttribute("city_id", city.getId());
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/employee/form.jsp").forward(request, response);
        return;                       
    }

    private void aShow(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("employe_id"));
        Employee employee = new EmployeeDAO().getEmployee(id);
        City city = new CityDAO().getCity(employee.getCity_id());
        request.setAttribute("objEmployee",employee);
        request.setAttribute("objUser",new UserDAO().getUser(employee.getUser_id()));
        request.setAttribute("city_name", city.getName());
        request.setAttribute("state", new StateDAO().getState(city.getState_id()));
        request.setAttribute("active_home","active" );
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/employee/show.jsp").forward(request, response);
        return;
    }

    private void aSave(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if ((request.getSession().getAttribute("submitIsOk").equals("true"))) {
             String msg = "";
             msg = validAttributes(request);
            if (msg.isEmpty()) {
                String employeName = request.getParameter("name");

                if (save(request)) {
                    msg = flashSucess("Funcionario " + employeName + " foi salvado com sucesso");
                    request.setAttribute("flash", msg);
                } else {
                    msg = flashError("Não foi possível salvar o funcionario: " + employeName);
                    request.setAttribute("flash", msg);
                }
                request.getSession().setAttribute("submitIsOk", "");
                aList(request, response);
                return;
            }else {
                recoveryDataForm(request);
                request.getSession().setAttribute("flash", flashError(msg));
                activeMenuLeft(request);
                request.getRequestDispatcher("/views/employee/form.jsp").forward(request, response);
                return;
            }
        }else{
         request.getSession().setAttribute("submitIsOk", "");
         aList(request, response);
     }
    }

    private void aList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("lstEmployees", new EmployeeDAO().getEmployees());
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/employee/list.jsp").forward(request, response);
        return;
    }

    private void aNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("lstStates", new StateDAO().getStates());
        request.setAttribute("objUser",new User());
        request.setAttribute("objEmployee", new Employee());
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/employee/form.jsp").forward(request, response);
        return;
    }

    private void aDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        String msg;
        
        boolean isOKUser=false;
        boolean isOKEmployee=false;
        
        EmployeeDAO employeDAO = new EmployeeDAO();
        
        int user_id = employeDAO.getEmployee(id).getUser_id(); 
        
        System.out.println("O  id do user que eu quero deletar e :"+String.valueOf(user_id));
        
        isOKEmployee = employeDAO.delete(id);
        isOKUser = new UserDAO().excluir(user_id);
        
        if (isOKUser && isOKEmployee) {
            msg = flashSucess("O Funcionário e o Usuário vinculado foram deletados com sucesso!!!");
        } else {
            msg = flashError("O funcionario ou usuário vinculado NÃO foi deletado.");
        }
        activeMenuLeft(request);
        request.getSession().setAttribute("flash", msg);
        aList(request, response);
    }

    private String validAttributes(HttpServletRequest request) {
        String msg = "";
        //DADOS GERAIS
        if (request.getParameter("name").isEmpty()) {
            msg += "<br />O <b>NOME</b> não foi preenchido.";
        }
        String gender = request.getParameter("gender");
        if (gender.isEmpty()||gender.equals("0")) {
            msg += "<br />Selecione um <b>GÊNERO</b> para o funcionário.";
        }
        if (request.getParameter("rg").isEmpty()) {
            msg += "<br />O <b>RG</b> não foi preenchido.";
        }
        if (request.getParameter("cpf").isEmpty()) {
            msg += "<br />O <b>CPF</b> não foi preenchido.";
        }
        if (request.getParameter("phone").isEmpty()) {
            msg += "<br />O <b>NUMERO DE TELEFONE</b> não foi preenchido.";
        }
        if (request.getParameter("dt_birth").isEmpty()) {
            msg += "<br />A <b>DATA DE NASCIMENTO</b> não foi preenchida.";
        }
        //ENDEREÇO
        if (request.getParameter("cep").isEmpty()) {
            msg += "<br />O <b>CEP</b> não foi preenchida.";
        }
        if (request.getParameter("street").isEmpty()) {
            msg += "<br />O <b>LOGRADOURO</b> não foi preenchido.";
        }
        if (request.getParameter("neighborhood").isEmpty()) {
            msg += "<br />O <b>BAIRRO</b> não foi preenchido.";
        }
        if (request.getParameter("number").isEmpty()) {
            msg += "<br />O <b>NUMERO</b> não foi preenchido.";
        }
        String state = request.getParameter("state_id");
        if (state.isEmpty()||state.equals("0")) {
            msg += "<br />O <b>ESTADO</b> não foi selecionado.";
        }
        String city = request.getParameter("city_id");
        if (city.isEmpty()||city.equals("0")) {
            msg += "<br />O <b>CIDADE</b> não foi selecionada.";
        }
        
        //DADOS DO USUÁRIO
        if (request.getParameter("user_login").isEmpty()) {
            msg += "<br />O <b>USUÁRIO</b> não foi preenchido.";
        }
         if (request.getParameter("user_password").isEmpty()) {
            msg += "<br />A <b>SENHA</b> não foi digitada.";
        } else {
            if (request.getParameter("user_password_confirmation").isEmpty()) {
                msg += "<br />Digite a <b>SENHA</b> novamente.";
            } else {
                if (!(request.getParameter("user_password_confirmation").
                        equals(request.getParameter("user_password")))) {
                    msg += "<br />As <b>SENHAS</b> não são iguais.";
                }
            }
        }

               return msg;
    }

    private void recoveryDataForm(HttpServletRequest request)throws IOException, ServletException{
        //USER EMPLOYE
        User user = new User();
        user.setName(request.getParameter("name"));
        user.setLogin(request.getParameter("user_login"));
        user.setPassword(request.getParameter("user_password"));
        user.setStatus(Boolean.parseBoolean(request.getParameter("user_status")));
        user.setAdmin(Boolean.parseBoolean(request.getParameter("user_admin")));
        
        //EMPLOYE
        Employee employee = new Employee();
        employee.setName(request.getParameter("name"));
        employee.setGender(request.getParameter("gender"));
        employee.setRg(request.getParameter("rg"));
        employee.setCpf(request.getParameter("cpf"));
        employee.setPhone(request.getParameter("phone"));

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            employee.setDt_birth(df.parse(request.getParameter("dt_birth")));
        } catch (ParseException ex) {
            Logger.getLogger(EmployeeAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        employee.setCep(request.getParameter("cep"));
        employee.setStreet(request.getParameter("street"));
        employee.setNeighborhood(request.getParameter("neighborhood"));
        employee.setNumber(request.getParameter("number"));
        employee.setCity_id(Integer.parseInt(request.getParameter("city_id")));
        
        String state_id = request.getParameter("state_id");
        String city_id = request.getParameter("city_id");
        
        request.setAttribute("objUser",user);
        request.setAttribute("lstCities", new CityDAO().getCities(Integer.parseInt(state_id)));
        request.setAttribute("lstStates", new StateDAO().getStates());
        request.setAttribute("state_id", state_id);
        request.setAttribute("city_id", city_id);
        request.setAttribute("objEmployee", employee);
    }

    private void activeMenuLeft(HttpServletRequest request)throws IOException, ServletException{
      request.setAttribute("active_employees", "active");
      request.setAttribute("open_administration", "block");  
    }
}
