/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hugoMed.action;

import br.com.hugoMed.bean.User;
import br.com.hugoMed.controller.BusinessLogic;
import br.com.hugoMed.persistence.HealthPlanTypeDAO;
import br.com.hugoMed.util.PopulateObject;
import br.com.hugoMed.util.CriptografiaMD5;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HealthPlanTypeAction implements BusinessLogic {

    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String action = request.getParameter("action");

        switch (action) {
            case "login": {
                aLogin(request, response);
                break;
            }
            case "logout": {
                aLogout(request, response);
                break;
            }
            case "index": {
                aIndex(request, response);
                break;
            }
            case "new": {
                aNew(request, response);
                break;
            }
            case "list": {
                aList(request, response);
                break;
            }
            case "save": {
                aSave(request, response);
                break;
            }
            case "show": {
                aShow(request, response);
                break;
            }
            case "edit": {
                aEdit(request, response);
                break;
            }
            case "delete": {
                aDelete(request, response);
                break;  
            }
        }
    }

    private boolean save(HttpServletRequest request) {
        User health_plan_type = (User) PopulateObject.createObjectBusiness(new User(), request);

        if (health_plan_type.getId() == 0) {
            health_plan_type.setPassword(CriptografiaMD5.encrypt(health_plan_type.getPassword()));
            return new HealthPlanTypeDAO().incluir(health_plan_type);
        } else {
            User health_plan_type = new HealthPlanTypeDAO().getUser(health_plan_type.getId());
            if (!health_plan_type.getPassword().equals(health_plan_type.getPassword())) {
                health_plan_type.setPassword(CriptografiaMD5.encrypt(health_plan_type.getPassword()));
            }
            return new HealthPlanTypeDAO().alterar(health_plan_type);
        }

    }

    private String flashError(String msg) {
        String mensagem = "<div class='alert alert-block alert-error'>"
                + "<button type = 'button' class='close' data-dismiss='alert'>"
                + "<i class='icon-remove'></i></button>"
                + "<p><strong><i class='icon-remove'></i> ERRO!!!  </strong>"
                + msg
                + "</p></div>";
        return mensagem;
    }

    private String flashSucess(String msg) {
        String mensagem = "<div class='alert alert-block alert-success'>"
                + "<button type = 'button' class='close' data-dismiss='alert'>"
                + "<i class='icon-remove'></i></button>"
                + "<p><strong><i class='icon-ok'></i> Sucesso!!!  </strong>"
                + msg
                + "</p></div>";
        return mensagem;
    }

    private String validAttributes(HttpServletRequest request) {
        String msg = "";
        if (request.getParameter("name").isEmpty()) {
            msg += "<br />O nome não foi preenchido.";
        }
        if (request.getParameter("login").isEmpty()) {
            msg += "<br />O login não foi preenchido.";
        }
        if (request.getParameter("password").isEmpty()) {
            msg += "<br />A senha não foi preenchida.";
        } else {
            if (!(request.getParameter("password").equals(request.getParameter("passwordconfirmation")))) {
                msg += "<br />As senhas não são correspondentes.";
            }
        }
        return msg;
    }

    private void aIndex(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/views/layout/application.jsp").forward(request, response);
        return;
    }

    private void aNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("objHealthPlanType", new User());
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/health_plan_type/form.jsp").forward(request, response);
        return;
    }

    private void aList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("lstHealthPlanTypes", new HealthPlanTypeDAO().getUsers());
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/health_plan_type/list.jsp").forward(request, response);
        return;
    }

    private void aSave(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

     if( (request.getSession().getAttribute("submitIsOk").equals("true"))){
        String msg = "";
        msg = validAttributes(request);
         if (msg.isEmpty()) {

            String userName = request.getParameter("name");

            if (save(request)) {
                msg = flashSucess("Tipo do plano " + userName + " foi salvado com sucesso");
                request.getSession().setAttribute("flash", msg);
            } else {
                msg = flashError("Não foi possível salvar o tipo do plano: " + userName);
                request.getSession().setAttribute("flash", msg);
            }
            request.setAttribute("lstHealthPlanTypes", new HealthPlanTypeDAO().getUsers());
            request.getSession().setAttribute("submitIsOk",false);
            aList(request, response);
            return;
       } else {
            User health_plan_type = new User();
            health_plan_type.setName(request.getParameter("name"));
            health_plan_type.setPassword(request.getParameter("password"));
            health_plan_type.setStatus(Boolean.parseBoolean(request.getParameter("status")));
            health_plan_type.setId(Integer.parseInt(request.getParameter("id")));
            health_plan_type.setLogin(request.getParameter("login"));
            health_plan_type.setAdmin(Boolean.parseBoolean(request.getParameter("admin")));
            request.setAttribute("objHealthPlanType", health_plan_type);
            request.getSession().setAttribute("flash", flashError(msg));
             activeMenuLeft(request);
            request.getRequestDispatcher("/views/health_plan_type/form.jsp").forward(request, response);
            return;
        }
    }else{
         request.getSession().setAttribute("submitIsOk", "");
         aList(request, response);
     }
    }

    private void aShow(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("objHealthPlanType", new HealthPlanTypeDAO().getUser(id));
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/health_plan_type/show.jsp").forward(request, response);
        return;
    }

    private void aEdit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("objHealthPlanType", new HealthPlanTypeDAO().getUser(id));
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/health_plan_type/form.jsp").forward(request, response);
        return;
    }

    private void aDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        String msg;
        if (new HealthPlanTypeDAO().excluir(id)) {
            msg = flashSucess("O tipo do plano foi deletado.");
        } else {
            msg = flashError("Tipo do plano NAO deletado pode ser deletado.");
        }
        request.getSession().setAttribute("flash", msg);
        aList(request, response);
    }
    private void activeMenuLeft(HttpServletRequest request)throws IOException, ServletException{
      request.setAttribute("active_health_plan_types", "active");
    }
}
