/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hugoMed.action;

import br.com.hugoMed.bean.State;
import br.com.hugoMed.controller.BusinessLogic;
import br.com.hugoMed.persistence.StateDAO;
import br.com.hugoMed.util.PopulateObject;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author josue
 */
public class StateAction implements BusinessLogic {

    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");

        switch (action) {

            case "new": {
                aNew(request, response);
                break;
            }
            case "list": {
                aList(request, response);
                break;
            }
            case "save": {
                aSave(request, response);
                break;
            }
            case "show": {
                aShow(request, response);
                break;
            }
            case "edit": {
                aEdit(request, response);
                break;
            }
            case "delete": {
                aDelete(request, response);
                break;
            }
        }
    }

    private boolean save(HttpServletRequest request) {
        State state = (State) PopulateObject.createObjectBusiness(new State(), request);

        if (state.getId() == 0) {
            return new StateDAO().create(state);
        } else {
            return new StateDAO().update(state);
        }
    }

    private String flashError(String msg) {
        String mensagem = "<div class='alert alert-block alert-error'>"
                + "<button type = 'button' class='close' data-dismiss='alert'>"
                + "<i class='icon-remove'></i></button>"
                + "<p><strong><i class='icon-remove'></i> ERRO!!!  </strong>"
                + msg
                + "</p></div>";
        return mensagem;
    }

    private String flashSucess(String msg) {
        String mensagem = "<div class='alert alert-block alert-success'>"
                + "<button type = 'button' class='close' data-dismiss='alert'>"
                + "<i class='icon-remove'></i></button>"
                + "<p><strong><i class='icon-ok'></i> Sucesso!!!  </strong>"
                + msg
                + "</p></div>";
        return mensagem;
    }

    private void aEdit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("objState", new StateDAO().getState(id));
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/state/form.jsp").forward(request, response);
        return;
    }

    private void aShow(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("objState", new StateDAO().getState(id));
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/state/show.jsp").forward(request, response);
        return;
    }

    private void aSave(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        if ((request.getSession().getAttribute("submitIsOk").equals("true"))) {
            
             String msg = "";
             msg = validAttributes(request);
             
            if (msg.isEmpty()) {
                String stateName = request.getParameter("name");

                if (save(request)) {
                    msg = flashSucess("Estado " + stateName + " foi salvado com sucesso");
                    request.setAttribute("flash", msg);
                } else {
                    msg = flashError("Não foi possível salvar o estado: " + stateName);
                    request.setAttribute("flash", msg);
                }
                request.getSession().setAttribute("submitIsOk", "");
                aList(request, response);
                return;
            }else {
                State state = new State();
                state.setName(request.getParameter("name"));
                state.setAcronym(request.getParameter("acronym"));
                request.getSession().setAttribute("flash", flashError(msg));
                activeMenuLeft(request);
                request.getRequestDispatcher("/views/state/form.jsp").forward(request, response);
                return;
            }
        }else{
         request.getSession().setAttribute("submitIsOk", "");
         aList(request, response);
     }
    }

    private void aList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("lstStates", new StateDAO().getStates());
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/state/list.jsp").forward(request, response);
        return;
    }

    private void aNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("objState", new State());
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/state/form.jsp").forward(request, response);
        return;
    }

    private void aDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        String msg;
        if (new StateDAO().delete(id)) {
            msg = flashSucess("O estado foi deletado.");
        } else {
            msg = flashError("Estado NAO pode ser deletado.");
        }
        request.getSession().setAttribute("flash", msg);
        aList(request, response);
    }

    private String validAttributes(HttpServletRequest request) {
        String msg = "";
        if (request.getParameter("name").isEmpty()) {
            msg += "<br />O nome não foi preenchido.";
        }
        if (request.getParameter("acronym").isEmpty()) {
            msg += "<br />A sigla não foi preenchida.";
        }
        return msg;
    }
    
    private void activeMenuLeft(HttpServletRequest request)throws IOException, ServletException{
      request.setAttribute("active_states_and_cities", "active");
      request.setAttribute("open_administration", "block");  
    }
}
