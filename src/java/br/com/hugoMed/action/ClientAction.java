/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hugoMed.action;

import br.com.hugoMed.bean.City;
import br.com.hugoMed.bean.Client;
import br.com.hugoMed.bean.HealthPlan;
import br.com.hugoMed.bean.User;
import br.com.hugoMed.controller.BusinessLogic;
import br.com.hugoMed.persistence.CityDAO;
import br.com.hugoMed.persistence.ClientDAO;
import br.com.hugoMed.persistence.HealthPlanDAO;
import br.com.hugoMed.persistence.StateDAO;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author josue
 */
public class ClientAction implements BusinessLogic {

  public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    String action = request.getParameter("action");

    switch (action) {

      case "new": {
        aNew(request, response);
        break;
      }
      case "list": {
        aList(request, response);
        break;
      }
      case "save": {
        aSave(request, response);
        break;
      }
      case "show": {
        aShow(request, response);
        break;
      }
      case "edit": {
        aEdit(request, response);
        break;
      }
      case "delete": {
        aDelete(request, response);
        break;
      }
    }
  }

  private boolean save(HttpServletRequest request) {
    Client client = new Client();

    client.setName(request.getParameter("name"));
    client.setGender(request.getParameter("gender"));
    client.setNumber(Integer.parseInt(request.getParameter("rg")));
    client.setCpf(request.getParameter("cpf"));
    client.setPhone(request.getParameter("phone"));
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    try {
      client.setDt_birth(df.parse(request.getParameter("dt_birth")));
    } catch (ParseException ex) {
      Logger.getLogger(ClientAction.class.getName()).log(Level.SEVERE, null, ex);
    }

    client.setCep(request.getParameter("cep"));
    client.setNeighborhood(request.getParameter("neighborhood"));
    client.setStreet(request.getParameter("street"));
    client.setNumber(Integer.parseInt(request.getParameter("number")));
    client.setCity_id(Integer.parseInt(request.getParameter("city_id")));

    HealthPlan health_plan = new HealthPlan();

    health_plan.setCard_number(Integer.parseInt(request.getParameter("card_number")));
    health_plan.setHealth_plan_type_id(Integer.parseInt(request.getParameter("health_plan_type_id")));
    health_plan.setClient_id(new ClientDAO().getClient(client.getCpf()).getId());
    try {
      health_plan.setDt_expiration(df.parse(request.getParameter("dt_expiration")));
    } catch (ParseException ex) {
      Logger.getLogger(ClientAction.class.getName()).log(Level.SEVERE, null, ex);
    }

    if (client.getId() == 0) {
      if (new ClientDAO().create(client)) {
        health_plan.setClient_id(new ClientDAO().getClient(client.getCpf()).getId());

        return new HealthPlanDAO().create(health_plan);
      } else {
        return false;
      }
    } else if (new ClientDAO().update(client)) {
      health_plan.setClient_id(new ClientDAO().getClient(client.getCpf()).getId());
      return new HealthPlanDAO().update(health_plan);
    } else {
      return false;
    }
  }

  private String flashError(String msg) {
    String mensagem = "<div class='alert alert-block alert-error'>"
            + "<button type = 'button' class='close' data-dismiss='alert'>"
            + "<i class='icon-remove'></i></button>"
            + "<p><strong><i class='icon-remove'></i> ERRO!!!  </strong>"
            + msg
            + "</p></div>";
    return mensagem;
  }

  private String flashSucess(String msg) {
    String mensagem = "<div class='alert alert-block alert-success'>"
            + "<button type = 'button' class='close' data-dismiss='alert'>"
            + "<i class='icon-remove'></i></button>"
            + "<p><strong><i class='icon-ok'></i> Sucesso!!!  </strong>"
            + msg
            + "</p></div>";
    return mensagem;
  }

  private void aEdit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    int id = Integer.parseInt(request.getParameter("id"));
    Client client = new ClientDAO().getClient(id);
    City city = new CityDAO().getCity(client.getCity_id());

    request.setAttribute("objClient", client);
    request.setAttribute("lstStates", new StateDAO().getStates());
    request.setAttribute("lstCities", new CityDAO().getCities(city.getState_id()));
    request.setAttribute("state_id", new StateDAO().getState(city.getState_id()).getId());
    request.setAttribute("city_id", city.getId());
    activeMenuLeft(request);
    request.getRequestDispatcher("/views/client/form.jsp").forward(request, response);
    return;
  }

  private void aShow(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    int id = Integer.parseInt(request.getParameter("id"));
    Client client = new ClientDAO().getClient(id);
    City city = new CityDAO().getCity(client.getCity_id());
    request.setAttribute("objClient", client);
    request.setAttribute("city_name", city.getName());
    request.setAttribute("state", new StateDAO().getState(city.getState_id()));
    request.setAttribute("active_home", "active");
    activeMenuLeft(request);
    request.getRequestDispatcher("/views/client/show.jsp").forward(request, response);
    return;
  }

  private void aSave(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    if ((request.getSession().getAttribute("submitIsOk").equals("true"))) {
      String msg = "";
      msg = validAttributes(request);
      if (msg.isEmpty()) {
        String clientName = request.getParameter("name");

        if (save(request)) {
          msg = flashSucess("Titular " + clientName + " foi salvado com sucesso");
          request.setAttribute("flash", msg);
        } else {
          msg = flashError("Não foi possível salvar o cliente: " + clientName);
          request.setAttribute("flash", msg);
        }
        request.getSession().setAttribute("submitIsOk", "");
        aList(request, response);
        return;
      } else {
        recoveryDataForm(request);
        request.getSession().setAttribute("flash", flashError(msg));
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/client/form.jsp").forward(request, response);
        return;
      }
    } else {
      request.getSession().setAttribute("submitIsOk", "");
      aList(request, response);
    }
  }

  private void aList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    request.setAttribute("lstClients", new ClientDAO().getClients());
    activeMenuLeft(request);
    request.getRequestDispatcher("/views/client/list.jsp").forward(request, response);
    return;
  }

  private void aNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    request.setAttribute("lstStates", new StateDAO().getStates());
    request.setAttribute("objUser", new User());
    request.setAttribute("objClient", new Client());
    activeMenuLeft(request);
    request.getRequestDispatcher("/views/client/form.jsp").forward(request, response);
    return;
  }

  private void aDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    int id = Integer.parseInt(request.getParameter("id"));
    String msg;

    boolean isOKClient = false;

    ClientDAO clientDAO = new ClientDAO();

    isOKClient = clientDAO.delete(id);

    if (isOKClient) {
      msg = flashSucess("O cliente foi deletado com sucesso!!!");
    } else {
      msg = flashError("O cliente NÃO foi deletado.");
    }
    activeMenuLeft(request);
    request.getSession().setAttribute("flash", msg);
    aList(request, response);
  }

  private String validAttributes(HttpServletRequest request) {
    String msg = "";
    //DADOS GERAIS
    if (request.getParameter("name").isEmpty()) {
      msg += "<br />O <b>NOME</b> não foi preenchido.";
    }
    String gender = request.getParameter("gender");
    if (gender.isEmpty() || gender.equals("0")) {
      msg += "<br />Selecione um <b>GÊNERO</b> para o cliente.";
    }
    if (request.getParameter("rg").isEmpty()) {
      msg += "<br />O <b>RG</b> não foi preenchido.";
    }
    if (request.getParameter("cpf").isEmpty()) {
      msg += "<br />O <b>CPF</b> não foi preenchido.";
    }
    if (request.getParameter("phone").isEmpty()) {
      msg += "<br />O <b>NUMERO DE TELEFONE</b> não foi preenchido.";
    }
    if (request.getParameter("dt_birth").isEmpty()) {
      msg += "<br />A <b>DATA DE NASCIMENTO</b> não foi preenchida.";
    }
    //ENDEREÇO
    if (request.getParameter("cep").isEmpty()) {
      msg += "<br />O <b>CEP</b> não foi preenchida.";
    }
    if (request.getParameter("street").isEmpty()) {
      msg += "<br />O <b>LOGRADOURO</b> não foi preenchido.";
    }
    if (request.getParameter("neighborhood").isEmpty()) {
      msg += "<br />O <b>BAIRRO</b> não foi preenchido.";
    }
    if (request.getParameter("number").isEmpty()) {
      msg += "<br />O <b>NUMERO</b> não foi preenchido.";
    }
    String state = request.getParameter("state_id");
    if (state.isEmpty() || state.equals("0")) {
      msg += "<br />O <b>ESTADO</b> não foi selecionado.";
    }
    String city = request.getParameter("city_id");
    if (city.isEmpty() || city.equals("0")) {
      msg += "<br />O <b>CIDADE</b> não foi selecionada.";
    }

    return msg;
  }

  private void recoveryDataForm(HttpServletRequest request) throws IOException, ServletException {
    //CLIENT
    Client client = new Client();
    client.setName(request.getParameter("name"));
    client.setGender(request.getParameter("gender"));
    client.setNumber(Integer.parseInt(request.getParameter("rg")));
    client.setCpf(request.getParameter("cpf"));
    client.setPhone(request.getParameter("phone"));

    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    try {
      client.setDt_birth(df.parse(request.getParameter("dt_birth")));
    } catch (ParseException ex) {
      Logger.getLogger(ClientAction.class.getName()).log(Level.SEVERE, null, ex);
    }

    client.setCep(request.getParameter("cep"));
    client.setStreet(request.getParameter("street"));
    client.setNeighborhood(request.getParameter("neighborhood"));
    client.setNumber(Integer.parseInt(request.getParameter("number")));
    client.setCity_id(Integer.parseInt(request.getParameter("city_id")));

    String state_id = request.getParameter("state_id");
    String city_id = request.getParameter("city_id");

    request.setAttribute("lstCities", new CityDAO().getCities(Integer.parseInt(state_id)));
    request.setAttribute("lstStates", new StateDAO().getStates());
    request.setAttribute("state_id", state_id);
    request.setAttribute("city_id", city_id);
    request.setAttribute("objClient", client);
  }

  private void activeMenuLeft(HttpServletRequest request) throws IOException, ServletException {
    request.setAttribute("active_clients", "active");
  }
}
