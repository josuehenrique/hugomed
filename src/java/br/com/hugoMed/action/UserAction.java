/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hugoMed.action;

import br.com.hugoMed.bean.User;
import br.com.hugoMed.controller.BusinessLogic;
import br.com.hugoMed.persistence.UserDAO;
import br.com.hugoMed.util.PopulateObject;
import br.com.hugoMed.util.CriptografiaMD5;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserAction implements BusinessLogic {

    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String action = request.getParameter("action");

        switch (action) {
            case "login": {
                aLogin(request, response);
                break;
            }
            case "logout": {
                aLogout(request, response);
                break;
            }
            case "index": {
                aIndex(request, response);
                break;
            }
            case "new": {
                aNew(request, response);
                break;
            }
            case "list": {
                aList(request, response);
                break;
            }
            case "save": {
                aSave(request, response);
                break;
            }
            case "show": {
                aShow(request, response);
                break;
            }
            case "edit": {
                aEdit(request, response);
                break;
            }
            case "delete": {
                aDelete(request, response);
                break;  
            }
        }
    }

    private boolean save(HttpServletRequest request) {
        User usuario = (User) PopulateObject.createObjectBusiness(new User(), request);

        if (usuario.getId() == 0) {
            usuario.setPassword(CriptografiaMD5.encrypt(usuario.getPassword()));
            return new UserDAO().incluir(usuario);
        } else {
            User user = new UserDAO().getUser(usuario.getId());
            if (!user.getPassword().equals(usuario.getPassword())) {
                usuario.setPassword(CriptografiaMD5.encrypt(usuario.getPassword()));
            }
            return new UserDAO().alterar(usuario);
        }

    }

    private String flashError(String msg) {
        String mensagem = "<div class='alert alert-block alert-error'>"
                + "<button type = 'button' class='close' data-dismiss='alert'>"
                + "<i class='icon-remove'></i></button>"
                + "<p><strong><i class='icon-remove'></i> ERRO!!!  </strong>"
                + msg
                + "</p></div>";
        return mensagem;
    }

    private String flashSucess(String msg) {
        String mensagem = "<div class='alert alert-block alert-success'>"
                + "<button type = 'button' class='close' data-dismiss='alert'>"
                + "<i class='icon-remove'></i></button>"
                + "<p><strong><i class='icon-ok'></i> Sucesso!!!  </strong>"
                + msg
                + "</p></div>";
        return mensagem;
    }

    private String validAttributes(HttpServletRequest request) {
        String msg = "";
        if (request.getParameter("name").isEmpty()) {
            msg += "<br />O nome não foi preenchido.";
        }
        if (request.getParameter("login").isEmpty()) {
            msg += "<br />O login não foi preenchido.";
        }
        if (request.getParameter("password").isEmpty()) {
            msg += "<br />A senha não foi preenchida.";
        } else {
            if (!(request.getParameter("password").equals(request.getParameter("passwordconfirmation")))) {
                msg += "<br />As senhas não são correspondentes.";
            }
        }
        return msg;
    }

    private void aLogin(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String login = request.getParameter("login");
        String senha = CriptografiaMD5.encrypt(request.getParameter("password"));

        User user = new UserDAO().getUser(login, senha);
        if (user == null) {
            request.setAttribute("msg", "Login e/ou senha incorretos!");
            request.getRequestDispatcher("/views/layout/index.jsp").forward(request, response);
            return;
        } else {
            if (user.isStatus()) {
                request.getSession().setAttribute("flash", flashSucess("Bem vindo ao HugoMed " + user.getName() + " !!!"));
                request.getSession().setAttribute("objUserSS", user);
                aIndex(request, response);
                return;
            } else {
                request.getSession().setAttribute("msg", "Usuário Inativo.");
                request.getRequestDispatcher("/views/layout/index.jsp").forward(request, response);
                return;
            }
        }
    }

    private void aLogout(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getSession().removeAttribute("objUsuarioSS");
        request.getRequestDispatcher("/views/layout/index.jsp").forward(request, response);
        return;
    }

    private void aIndex(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/views/layout/application.jsp").forward(request, response);
        return;
    }

    private void aNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("objUser", new User());
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/user/form.jsp").forward(request, response);
        return;
    }

    private void aList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("lstUsers", new UserDAO().getUsers());
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/user/list.jsp").forward(request, response);
        return;
    }

    private void aSave(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

     if( (request.getSession().getAttribute("submitIsOk").equals("true"))){
        String msg = "";
        msg = validAttributes(request);
         if (msg.isEmpty()) {

            String userName = request.getParameter("name");

            if (save(request)) {
                msg = flashSucess("Usuário " + userName + " foi salvado com sucesso");
                request.getSession().setAttribute("flash", msg);
            } else {
                msg = flashError("Não foi possível salvar o usuário: " + userName);
                request.getSession().setAttribute("flash", msg);
            }
            request.setAttribute("lstUsers", new UserDAO().getUsers());
            request.getSession().setAttribute("submitIsOk",false);
            aList(request, response);
            return;
       } else {
            User user = new User();
            user.setName(request.getParameter("name"));
            user.setPassword(request.getParameter("password"));
            user.setStatus(Boolean.parseBoolean(request.getParameter("status")));
            user.setId(Integer.parseInt(request.getParameter("id")));
            user.setLogin(request.getParameter("login"));
            user.setAdmin(Boolean.parseBoolean(request.getParameter("admin")));
            request.setAttribute("objUser", user);
            request.getSession().setAttribute("flash", flashError(msg));
             activeMenuLeft(request);
            request.getRequestDispatcher("/views/user/form.jsp").forward(request, response);
            return;
        }
    }else{
         request.getSession().setAttribute("submitIsOk", "");
         aList(request, response);
     }
    }

    private void aShow(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("objUser", new UserDAO().getUser(id));
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/user/show.jsp").forward(request, response);
        return;
    }

    private void aEdit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("objUser", new UserDAO().getUser(id));
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/user/form.jsp").forward(request, response);
        return;
    }

    private void aDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        String msg;
        if (new UserDAO().excluir(id)) {
            msg = flashSucess("O usuário foi deletado.");
        } else {
            msg = flashError("Usuário NAO deletado pode ser deletado.");
        }
        request.getSession().setAttribute("flash", msg);
        aList(request, response);
    }
    private void activeMenuLeft(HttpServletRequest request)throws IOException, ServletException{
      request.setAttribute("active_users", "active");
      request.setAttribute("open_administration", "block");  
    }
}
