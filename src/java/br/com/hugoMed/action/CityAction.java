/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hugoMed.action;

import br.com.hugoMed.bean.City;
import br.com.hugoMed.controller.BusinessLogic;
import br.com.hugoMed.persistence.CityDAO;
import br.com.hugoMed.persistence.StateDAO;
import br.com.hugoMed.util.PopulateObject;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author josue
 */
public class CityAction implements BusinessLogic {

    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");
        request.setCharacterEncoding("UTF-8");
        
        switch (action) {

            case "new": {
                aNew(request, response);
                break;
            }
            case "list": {
                aList(request, response);
                break;
            }
            case "save": {
                aSave(request, response);
                break;
            }
            case "show": {
                aShow(request, response);
                break;
            }
            case "edit": {
                aEdit(request, response);
                break;
            }
            case "delete": {
                aDelete(request, response);
                break;
            }
            case "getCities": {
                aGetCities(request, response);
                break;
            }
        }
    }

    private void aDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        String msg;
        if (new CityDAO().delete(id)) {
            msg = flashSucess("A cidade foi deletada.");
        } else {
            msg = flashError("Cidade NAO pode ser deletada.");
        }
        activeMenuLeft(request);
        request.getSession().setAttribute("flash", msg);
        aList(request, response);
    }

    private void aEdit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("objCity", new CityDAO().getCity(id));
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/city/form.jsp").forward(request, response);
        return;
    }

    private void aShow(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("objCity", new CityDAO().getCity(id));
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/city/show.jsp").forward(request, response);
        return;
    }

    private void aSave(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        if ((request.getSession().getAttribute("submitIsOk").equals("true"))) {

            String msg = "";
            msg = validAttributes(request);

            if (msg.isEmpty()) {
                String cityName = request.getParameter("name");

                if (save(request)) {
                    msg = flashSucess("Cidade " + cityName + " foi salvada com sucesso");
                    request.setAttribute("flash", msg);
                } else {
                    msg = flashError("Não foi possível salvar a cidade: " + cityName);
                    request.setAttribute("flash", msg);
                }
                request.getSession().setAttribute("submitIsOk", "");
                aList(request, response);
                return;
            } else {
                City city = new City();
                city.setName(request.getParameter("name"));
                request.getSession().setAttribute("flash", flashError(msg));
                activeMenuLeft(request);
                request.getRequestDispatcher("/views/city/form.jsp").forward(request, response);
                return;
            }
        } else {
            request.getSession().setAttribute("submitIsOk", "");
            aList(request, response);
        }
    }

    private void aList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("stateName", new StateDAO().getState(Integer.parseInt(request.getParameter("state_id"))).getName());
        request.setAttribute("lstCities", new CityDAO().getCities(Integer.parseInt(request.getParameter("state_id"))));
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/city/list.jsp").forward(request, response);
        return;
    }

    private void aNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        City city = new City();
        city.setState_id(Integer.parseInt(request.getParameter("state_id")));
        request.setAttribute("objCity", city);
        activeMenuLeft(request);
        request.getRequestDispatcher("/views/city/form.jsp").forward(request, response);
        return;
    }

    private void aGetCities(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
      request.setAttribute("listCities", new CityDAO().getCities(Integer.parseInt(request.getParameter("state"))));
      request.getRequestDispatcher("/views/city/select_cities.jsp").forward(request, response);
      return;
    }

    private boolean save(HttpServletRequest request) {
        City city = (City) PopulateObject.createObjectBusiness(new City(), request);

        if (city.getId() == 0) {
            return new CityDAO().create(city);
        } else {
            return new CityDAO().update(city);
        }
    }


    private String flashError(String msg) {
        String mensagem = "<div class='alert alert-block alert-error'>"
                + "<button type = 'button' class='close' data-dismiss='alert'>"
                + "<i class='icon-remove'></i></button>"
                + "<p><strong><i class='icon-remove'></i> ERRO!!!  </strong>"
                + msg
                + "</p></div>";
        return mensagem;
    }

    private String flashSucess(String msg) {
        String mensagem = "<div class='alert alert-block alert-success'>"
                + "<button type = 'button' class='close' data-dismiss='alert'>"
                + "<i class='icon-remove'></i></button>"
                + "<p><strong><i class='icon-ok'></i> Sucesso!!!  </strong>"
                + msg
                + "</p></div>";
        return mensagem;
    }
     
    private String validAttributes(HttpServletRequest request) {
        String msg = "";
        if (request.getParameter("name").isEmpty()) {
            msg += "O nome não foi preenchido.";
        }
        return msg;
    }
        
    private void activeMenuLeft(HttpServletRequest request)throws IOException, ServletException{
      request.setAttribute("active_states_and_cities", "active");
      request.setAttribute("open_administration", "block");  
    }
}
