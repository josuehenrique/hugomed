/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.hugoMed.persistence;

import br.com.hugoMed.bean.City;
import br.com.hugoMed.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josue
 */
public class CityDAO {
      private String sql;
    
    public boolean create(City city) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "INSERT INTO cities(name,state_id) "
                + " VALUES(?,?);";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, city.getName());
            ps.setInt(2, city.getState_id());
            
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(CityDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public boolean update(City city) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "UPDATE cities SET name=?, state_id=? "
                + "WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, city.getName());
            ps.setInt(2, city.getState_id());
            ps.setInt(3, city.getId());
            
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(CityDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public boolean delete(int id) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "DELETE FROM cities WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setInt(1, id);
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(CityDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public City getCity(int id) {
        City city = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return city;
        }
        sql = "SELECT * FROM cities WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setInt(1, id);
            
            rs = ps.executeQuery();
            if(rs.next()){
                city = new City();
                city.setId(rs.getInt("id"));
                city.setName(rs.getString("name"));
                city.setState_id(rs.getInt("state_id"));
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(CityDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return city;
    }

    public List<City> getCities(int state_id) {
        List<City> cities = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return cities;
        }
        sql =     "SELECT * FROM cities WHERE state_id=? "
                + "ORDER BY name;";
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, state_id);
                        
            rs = ps.executeQuery();
            cities = new ArrayList<City>();
            while(rs.next()){
                City city = new City();
                city.setId(rs.getInt("id"));
                city.setName(rs.getString("name"));
                city.setState_id(rs.getInt("state_id"));
                
                cities.add(city);
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(CityDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return cities;
    }  
}
