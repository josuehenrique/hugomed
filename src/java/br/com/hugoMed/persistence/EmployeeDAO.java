/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.hugoMed.persistence;

import br.com.hugoMed.bean.Employee;
import br.com.hugoMed.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josue
 */
public class EmployeeDAO {
    
    private String sql;
    
    public boolean create(Employee employee) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "INSERT INTO employees(name, rg, cpf, cep, street, neighborhood, number,"
                + " phone, dt_birth, gender, city_id, user_id) "
                + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?);";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, employee.getName());
            ps.setInt(2, Integer.parseInt(employee.getRg()));
            ps.setString(3, employee.getCpf());
            ps.setString(4, employee.getCep());
            ps.setString(5, employee.getStreet());
            ps.setString(6, employee.getNeighborhood());
            ps.setInt(7, Integer.parseInt(employee.getNumber()));
            ps.setString(8, employee.getPhone());
            ps.setDate(9, new java.sql.Date(employee.getDt_birth().getTime()));
            ps.setString(10, employee.getGender());
            ps.setInt(11, employee.getCity_id());
            ps.setInt(12, employee.getUser_id());
            
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public boolean update(Employee employee) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "UPDATE employees SET name=?, rg=?, cpf=?, cep=?, street=?, neighborhood=?, number=?,"
                + " phone=?, dt_birth=?, gender=?, city=?, user_id=?"
                + "WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);
            
            ps.setString(1, employee.getName());
            ps.setInt(2, Integer.parseInt(employee.getRg()));
            ps.setString(3, employee.getCpf());
            ps.setString(4, employee.getCep());
            ps.setString(5, employee.getStreet());
            ps.setString(6, employee.getNeighborhood());
            ps.setInt(7, Integer.parseInt(employee.getNumber()));
            ps.setString(8, employee.getPhone());
            ps.setDate(9, new java.sql.Date(employee.getDt_birth().getTime()));
            ps.setString(10, employee.getGender());
            ps.setInt(11, employee.getCity_id());
            ps.setInt(12, employee.getUser_id());
            
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public boolean delete(int id) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "DELETE FROM employees WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setInt(1, id);
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public Employee getEmployee(int id) {
        Employee employee = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return employee;
        }
        sql = "SELECT * FROM employees WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setInt(1, id);
            
            rs = ps.executeQuery();
            if(rs.next()){
                employee = new Employee();
           
                employee.setId(rs.getInt("id"));
                employee.setName(rs.getString("name"));
                employee.setRg(String.valueOf(rs.getInt("rg")));
                employee.setCpf(rs.getString("cpf"));
                employee.setCep(rs.getString("cep"));
                employee.setStreet(rs.getString("street"));
                employee.setNeighborhood(rs.getString("neighborhood"));
                employee.setNumber(String.valueOf(rs.getInt("number")));
                employee.setPhone(rs.getString("phone"));
                employee.setDt_birth(rs.getDate("dt_birth"));                
                employee.setGender(rs.getString("gender"));
                employee.setCity_id(rs.getInt("city_id"));
                employee.setUser_id(rs.getInt("user_id"));
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return employee;
    }

    public List<Employee> getEmployees() {
        List<Employee> employees = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return employees;
        }
        sql = "SELECT * FROM employees ORDER BY name;";
        try {
            ps = conn.prepareStatement(sql);            
                        
            rs = ps.executeQuery();
            employees = new ArrayList<Employee>();
            while(rs.next()){
                Employee employee = new Employee();
                
                employee.setId(rs.getInt("id"));
                employee.setName(rs.getString("name"));
                employee.setRg(String.valueOf(rs.getInt("rg")));
                employee.setCpf(rs.getString("cpf"));
                employee.setCep(rs.getString("cep"));
                employee.setStreet(rs.getString("street"));
                employee.setNeighborhood(rs.getString("neighborhood"));
                employee.setNumber(String.valueOf(rs.getInt("number")));
                employee.setPhone(rs.getString("phone"));
                employee.setDt_birth(rs.getDate("dt_birth"));                
                employee.setGender(rs.getString("gender"));
                employee.setCity_id(rs.getInt("city_id"));
                employee.setUser_id(rs.getInt("user_id"));
                
                employees.add(employee);
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return employees;
    }
}
