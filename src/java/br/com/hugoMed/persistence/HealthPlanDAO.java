/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hugoMed.persistence;

import br.com.hugoMed.bean.HealthPlan;
import br.com.hugoMed.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josue
 */
public class HealthPlanDAO {

  private String sql;

  public boolean create(HealthPlan health_plan) {
    boolean isOk = false;
    Connection conn;
    PreparedStatement ps = null;

    conn = ConnectionFactory.getConnection();
    if (conn == null) {
      return isOk;
    }
    sql = "INSERT INTO health_plans(card_number, dt_expiration, active, health_plan_type_id, client_id) "
            + " VALUES(?,?,?,?,?);";
    try {
      ps = conn.prepareStatement(sql);

      ps.setInt(1, health_plan.getCard_number());
      ps.setDate(2, new java.sql.Date(health_plan.getDt_expiration().getTime()));
      ps.setBoolean(3, health_plan.getActive());
      ps.setInt(4, health_plan.getHealth_plan_type_id());
      ps.setInt(5, health_plan.getClient_id());

      ps.executeUpdate();
      isOk = true;
    } catch (SQLException ex) {
      Logger.getLogger(HealthPlanDAO.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      ConnectionFactory.closeConnection(conn, ps);
    }

    return isOk;
  }

  public boolean update(HealthPlan health_plan) {
    boolean isOk = false;
    Connection conn;
    PreparedStatement ps = null;

    conn = ConnectionFactory.getConnection();
    if (conn == null) {
      return isOk;
    }
    sql = "UPDATE health_plans SET card_number=?, dt_expiration=?, active=?, client_id=?, health_plan_type_id=?, "
            + " client_id=? WHERE id=?;";
    try {
      ps = conn.prepareStatement(sql);

      ps.setInt(1, health_plan.getCard_number());
      ps.setDate(2, new java.sql.Date(health_plan.getDt_expiration().getTime()));
      ps.setBoolean(3, health_plan.getActive());
      ps.setInt(4, health_plan.getHealth_plan_type_id());
      ps.setInt(5, health_plan.getClient_id());

      ps.executeUpdate();
      isOk = true;
    } catch (SQLException ex) {
      Logger.getLogger(HealthPlanDAO.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      ConnectionFactory.closeConnection(conn, ps);
    }

    return isOk;
  }

  public boolean delete(int id) {
    boolean isOk = false;
    Connection conn;
    PreparedStatement ps = null;

    conn = ConnectionFactory.getConnection();
    if (conn == null) {
      return isOk;
    }
    sql = "DELETE FROM health_plans WHERE id=?;";
    try {
      ps = conn.prepareStatement(sql);
      ps.setInt(1, id);
      ps.executeUpdate();
      isOk = true;
    } catch (SQLException ex) {
      Logger.getLogger(HealthPlanDAO.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      ConnectionFactory.closeConnection(conn, ps);
    }

    return isOk;
  }

  public HealthPlan getHealthPlan(int id) {
    HealthPlan health_plan = null;
    Connection conn;
    PreparedStatement ps = null;
    ResultSet rs = null;

    conn = ConnectionFactory.getConnection();
    if (conn == null) {
      return health_plan;
    }
    sql = "SELECT * FROM health_plans WHERE id=?;";
    try {
      ps = conn.prepareStatement(sql);
      ps.setInt(1, id);

      rs = ps.executeQuery();
      if (rs.next()) {
        health_plan = new HealthPlan();

        health_plan.setId(rs.getInt("id"));
        health_plan.setCard_number(rs.getInt("card_number"));
        health_plan.setHealth_plan_type_id(rs.getInt("health_plan_type_id"));
        health_plan.setClient_id(rs.getInt("client_id"));
        health_plan.setDt_expiration(rs.getDate("dt_expiration"));
        health_plan.setActive(rs.getBoolean("active"));
      }

    } catch (SQLException ex) {
      Logger.getLogger(HealthPlanDAO.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      ConnectionFactory.closeConnection(conn, ps, rs);
    }

    return health_plan;
  }
}
