/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.hugoMed.persistence;

import br.com.hugoMed.bean.State;
import br.com.hugoMed.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josue
 */
public class StateDAO {
    
    private String sql;
    
    public boolean create(State state) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "INSERT INTO states(name, acronym) "
                + " VALUES(?,?);";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, state.getName());
            ps.setString(2, state.getAcronym());
            
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(StateDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public boolean update(State state) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "UPDATE states SET name=?, acronym=? "
                + "WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, state.getName());
            ps.setString(2, state.getAcronym());
            ps.setInt(3, state.getId());
            
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(StateDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public boolean delete(int id) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "DELETE FROM states WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setInt(1, id);
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(StateDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public State getState(int id) {
        State state = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return state;
        }
        sql = "SELECT * FROM states WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setInt(1, id);
            
            rs = ps.executeQuery();
            if(rs.next()){
                state = new State();
                state.setId(rs.getInt("id"));
                state.setName(rs.getString("name"));
                state.setAcronym(rs.getString("acronym"));
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(StateDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return state;
    }

    public List<State> getStates() {
        List<State> states = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return states;
        }
        sql = "SELECT * FROM states ORDER BY name;";
        try {
            ps = conn.prepareStatement(sql);            
                        
            rs = ps.executeQuery();
            states = new ArrayList<State>();
            while(rs.next()){
                State state = new State();
                state.setId(rs.getInt("id"));
                state.setName(rs.getString("name"));
                state.setAcronym(rs.getString("acronym"));
                
                states.add(state);
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(StateDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return states;
    }
    
}
