/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hugoMed.persistence;

import br.com.hugoMed.bean.User;
import br.com.hugoMed.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josue
 */
public class UserDAO {

    private String sql;

    public User getUser(String login, String senha) {
        User usuario = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return usuario;
        }
        sql = "SELECT * FROM users WHERE login=? AND password=?;";

        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, login);
            ps.setString(2, senha);
     
            rs = ps.executeQuery();

            if (rs.next()) {
                usuario = new User();
                usuario.setId(rs.getInt("id"));
                usuario.setName(rs.getString("name"));
                usuario.setLogin(rs.getString("login"));
                usuario.setPassword(rs.getString("password"));
                usuario.setStatus(rs.getBoolean("status"));
                usuario.setAdmin(rs.getBoolean("admin"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return usuario;
    }

    public boolean incluir(User usuario) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "INSERT INTO users(name, login, password, status, admin) "
                + " VALUES(?,?,?,?,?);";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, usuario.getName());
            ps.setString(2, usuario.getLogin());
            ps.setString(3, usuario.getPassword());
            ps.setBoolean(4, usuario.isStatus());
            ps.setBoolean(5, usuario.isAdmin());
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public boolean alterar(User usuario) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "UPDATE users SET name=?, login=?, password=?, status=?, admin=? "
                + "WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, usuario.getName());
            ps.setString(2, usuario.getLogin());
            ps.setString(3, usuario.getPassword());
            ps.setBoolean(4, usuario.isStatus());
            ps.setBoolean(5, usuario.isAdmin());
            ps.setInt(6, usuario.getId());
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public boolean excluir(int id) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "DELETE FROM users WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setInt(1, id);
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public User getUser(int id) {
        User usuario = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return usuario;
        }
        sql = "SELECT * FROM users WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setInt(1, id);
            
            rs = ps.executeQuery();
            if(rs.next()){
                usuario = new User();
                usuario.setId(rs.getInt("id"));
                usuario.setName(rs.getString("name"));
                usuario.setLogin(rs.getString("login"));
                usuario.setPassword(rs.getString("password"));
                usuario.setStatus(rs.getBoolean("status"));
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return usuario;
    }

    public List<User> getUsers() {
        List<User> usuarios = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return usuarios;
        }
        sql = "SELECT * FROM users ORDER BY name;";
        try {
            ps = conn.prepareStatement(sql);            
                        
            rs = ps.executeQuery();
            usuarios = new ArrayList<User>();
            while(rs.next()){
                User usuario = new User();
                usuario.setId(rs.getInt("id"));
                usuario.setName(rs.getString("name"));
                usuario.setLogin(rs.getString("login"));
                usuario.setPassword(rs.getString("password"));
                usuario.setStatus(rs.getBoolean("status"));
                usuario.setAdmin(rs.getBoolean("admin"));
                usuarios.add(usuario);
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return usuarios;
    }
    public int getUserId(String name, String login){
        int user_id=0;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return 0;
        }
        sql = "SELECT id FROM users WHERE name=? AND login=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setString(1, name);
            ps.setString(2, login);
            
            rs = ps.executeQuery();
            if(rs.next()){
                user_id = rs.getInt("id");
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return user_id;
    }
 
}
