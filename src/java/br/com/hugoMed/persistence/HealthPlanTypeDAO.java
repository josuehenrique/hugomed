/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hugoMed.persistence;

import br.com.hugoMed.bean.HealthPlanType;
import br.com.hugoMed.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josue
 */
public class HealthPlanTypeDAO {

  private String sql;

  public boolean create(HealthPlanType health_plan_type) {
    boolean isOk = false;
    Connection conn;
    PreparedStatement ps = null;

    conn = ConnectionFactory.getConnection();
    if (conn == null) {
      return isOk;
    }
    sql = "INSERT INTO health_plan_types(name, dt_creation) "
            + " VALUES(?,?);";
    try {
      ps = conn.prepareStatement(sql);

      ps.setString(1, health_plan_type.getName());
      ps.setDate(2, new java.sql.Date(health_plan_type.getDt_creation().getTime()));

      ps.executeUpdate();
      isOk = true;
    } catch (SQLException ex) {
      Logger.getLogger(HealthPlanTypeDAO.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      ConnectionFactory.closeConnection(conn, ps);
    }

    return isOk;
  }

  public boolean update(HealthPlanType health_plan_type) {
    boolean isOk = false;
    Connection conn;
    PreparedStatement ps = null;

    conn = ConnectionFactory.getConnection();
    if (conn == null) {
      return isOk;
    }
    sql = "UPDATE health_plan_types SET name=?, dt_creation=? "
            + " WHERE id=?;";
    try {
      ps = conn.prepareStatement(sql);

       ps.setString(1, health_plan_type.getName());
      ps.setDate(2, new java.sql.Date(health_plan_type.getDt_creation().getTime()));

      ps.executeUpdate();
      isOk = true;
    } catch (SQLException ex) {
      Logger.getLogger(HealthPlanTypeDAO.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      ConnectionFactory.closeConnection(conn, ps);
    }

    return isOk;
  }

  public boolean delete(int id) {
    boolean isOk = false;
    Connection conn;
    PreparedStatement ps = null;

    conn = ConnectionFactory.getConnection();
    if (conn == null) {
      return isOk;
    }
    sql = "DELETE FROM health_plan_types WHERE id=?;";
    try {
      ps = conn.prepareStatement(sql);
      ps.setInt(1, id);
      ps.executeUpdate();
      isOk = true;
    } catch (SQLException ex) {
      Logger.getLogger(HealthPlanTypeDAO.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      ConnectionFactory.closeConnection(conn, ps);
    }

    return isOk;
  }

  public HealthPlanType getHealthPlan(int id) {
    HealthPlanType health_plan_type = null;
    Connection conn;
    PreparedStatement ps = null;
    ResultSet rs = null;

    conn = ConnectionFactory.getConnection();
    if (conn == null) {
      return health_plan_type;
    }
    sql = "SELECT * FROM health_plan_types WHERE id=?;";
    try {
      ps = conn.prepareStatement(sql);
      ps.setInt(1, id);

      rs = ps.executeQuery();
      if (rs.next()) {
        health_plan_type = new HealthPlanType();

        health_plan_type.setId(rs.getInt("id"));
        health_plan_type.setName(rs.getString("name"));
        health_plan_type.setDt_creation(rs.getDate("dt_creation"));
      }

    } catch (SQLException ex) {
      Logger.getLogger(HealthPlanTypeDAO.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      ConnectionFactory.closeConnection(conn, ps, rs);
    }

    return health_plan_type;
  }
}
