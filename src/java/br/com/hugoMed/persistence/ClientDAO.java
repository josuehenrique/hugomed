/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.hugoMed.persistence;

import br.com.hugoMed.bean.Client;
import br.com.hugoMed.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josue
 */
public class ClientDAO {
     private String sql;
    
    public boolean create(Client client) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "INSERT INTO clients(name, rg, cpf, cep, street, neighborhood, number,"
                + " phone, dt_birth, gender, city_id) "
                + " VALUES(?,?,?,?,?,?,?,?,?,?,?);";
        try {
            ps = conn.prepareStatement(sql);
          
            ps.setString(1, client.getName());
            ps.setInt(2, client.getRg());
            ps.setString(3, client.getCpf());
            ps.setString(4, client.getCep());
            ps.setString(5, client.getStreet());
            ps.setString(6, client.getNeighborhood());
            ps.setInt(7, client.getNumber());
            ps.setString(8, client.getPhone());
            ps.setDate(9, new java.sql.Date(client.getDt_birth().getTime()));
            ps.setString(10, client.getGender());
            ps.setInt(11, client.getCity_id());

            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public boolean update(Client client) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "UPDATE clients SET name=?, rg=?, cpf=?, cep=?, street=?, neighborhood=?, number=?,"
                + " phone=?, dt_birth=?, gender=?, city=?"
                + "WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);
            
            ps.setString(1, client.getName());
            ps.setInt(2, client.getRg());
            ps.setString(3, client.getCpf());
            ps.setString(4, client.getCep());
            ps.setString(5, client.getStreet());
            ps.setString(6, client.getNeighborhood());
            ps.setInt(7, client.getNumber());
            ps.setString(8, client.getPhone());
            ps.setDate(9, new java.sql.Date(client.getDt_birth().getTime()));
            ps.setString(10, client.getGender());
            ps.setInt(11, client.getCity_id());
            
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public boolean delete(int id) {
        boolean isOk = false;
        Connection conn;
        PreparedStatement ps = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return isOk;
        }
        sql = "DELETE FROM clients WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setInt(1, id);
            ps.executeUpdate();
            isOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps);
        }

        return isOk;
    }

    public Client getClient(int id) {
        Client client = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return client;
        }
        sql = "SELECT * FROM clients WHERE id=?;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setInt(1, id);
            
            rs = ps.executeQuery();
            if(rs.next()){
                client = new Client();
    
                client.setId(rs.getInt("id"));
                client.setName(rs.getString("name"));
                client.setRg(rs.getInt("rg"));
                client.setCpf(rs.getString("cpf"));
                client.setCep(rs.getString("cep"));
                client.setStreet(rs.getString("street"));
                client.setNeighborhood(rs.getString("neighborhood"));
                client.setNumber(rs.getInt("number"));
                client.setPhone(rs.getString("phone"));
                client.setDt_birth(rs.getDate("dt_birth"));                
                client.setGender(rs.getString("gender"));
                client.setCity_id(rs.getInt("city_id"));
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return client;
    }

    public Client getClient(String cpf) {
        Client client = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return client;
        }
        sql = "SELECT * FROM clients WHERE cpf=? limit 1;";
        try {
            ps = conn.prepareStatement(sql);            
            ps.setString(1, cpf);
            
            rs = ps.executeQuery();
            if(rs.next()){
                client = new Client();
    
                client.setId(rs.getInt("id"));
                client.setName(rs.getString("name"));
                client.setRg(rs.getInt("rg"));
                client.setCpf(rs.getString("cpf"));
                client.setCep(rs.getString("cep"));
                client.setStreet(rs.getString("street"));
                client.setNeighborhood(rs.getString("neighborhood"));
                client.setNumber(rs.getInt("number"));
                client.setPhone(rs.getString("phone"));
                client.setDt_birth(rs.getDate("dt_birth"));                
                client.setGender(rs.getString("gender"));
                client.setCity_id(rs.getInt("city_id"));
            }            
        } catch (SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return client;
    }

    public List<Client> getClients() {
        List<Client> clients = null;
        Connection conn;
        PreparedStatement ps = null;
        ResultSet rs = null;

        conn = ConnectionFactory.getConnection();
        if (conn == null) {
            return clients;
        }
        sql = "SELECT * FROM clients ORDER BY name;";
        try {
            ps = conn.prepareStatement(sql);            
                        
            rs = ps.executeQuery();
            clients = new ArrayList<Client>();
            while(rs.next()){
                Client client = new Client();
                
                client.setId(rs.getInt("id"));
                client.setName(rs.getString("name"));
                client.setRg(rs.getInt("rg"));
                client.setCpf(rs.getString("cpf"));
                client.setCep(rs.getString("cep"));
                client.setStreet(rs.getString("street"));
                client.setNeighborhood(rs.getString("neighborhood"));
                client.setNumber(rs.getInt("number"));
                client.setPhone(rs.getString("phone"));
                client.setDt_birth(rs.getDate("dt_birth"));                
                client.setGender(rs.getString("gender"));
                client.setCity_id(rs.getInt("city_id"));
                
                clients.add(client);
            }            
            
        } catch (SQLException ex) {
            Logger.getLogger(ClientDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            ConnectionFactory.closeConnection(conn, ps, rs);
        }

        return clients;
    }
}
