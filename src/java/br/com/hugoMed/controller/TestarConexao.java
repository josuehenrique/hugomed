/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.hugoMed.controller;

import br.com.hugoMed.connection.ConnectionFactory;
import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class TestarConexao extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        Connection conn = ConnectionFactory.getConnection();
        if (conn != null) {
            System.out.println("Conexao estabelecida");
        } else {
            System.out.println("Erro ao conectar ao banco de dados.");
        }

    }
}
