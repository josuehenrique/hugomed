/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.hugoMed.bean;

import java.util.Date;

/**
 *
 * @author josue
 */
public class HealthPlan {
    private int id;
    private int card_number;
    private int health_plan_type_id;
    private int client_id;
    private Date dt_expiration;
    private Boolean active;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getCard_number() {
    return card_number;
  }

  public void setCard_number(int card_number) {
    this.card_number = card_number;
  }

  public int getHealth_plan_type_id() {
    return health_plan_type_id;
  }

  public void setHealth_plan_type_id(int health_plan_type_id) {
    this.health_plan_type_id = health_plan_type_id;
  }

  public int getClient_id() {
    return client_id;
  }

  public void setClient_id(int client_id) {
    this.client_id = client_id;
  }

  public Date getDt_expiration() {
    return dt_expiration;
  }

  public void setDt_expiration(Date dt_expiration) {
    this.dt_expiration = dt_expiration;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

}
