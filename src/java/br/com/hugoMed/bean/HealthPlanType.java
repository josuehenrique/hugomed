/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.hugoMed.bean;

import java.util.Date;

/**
 *
 * @author josue
 */
public class HealthPlanType {
    private int id;
    private String name;
    private Date dt_creation;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the dt_creation
     */
    public Date getDt_creation() {
        return dt_creation;
    }

    /**
     * @param dt_creation the dt_creation to set
     */
    public void setDt_creation(Date dt_creation) {
        this.dt_creation = dt_creation;
    }
}
