/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.hugoMed.util;

import br.com.hugoMed.bean.City;
import br.com.hugoMed.bean.State;
import br.com.hugoMed.persistence.CityDAO;
import java.util.List;

/**
 *
 * @author josue
 */
public class Selects {
    
   public List<City> getCities(State state) {  
        CityDAO daoCity = new CityDAO();  
        List<City> listCities = new java.util.ArrayList<City>();  
        listCities = daoCity.getCities(state.getId());  
        return listCities;  
    }   
}
